namespace AppServices
{
    /// <summary>
    /// Authentication options.
    /// </summary>
    public sealed class AuthenticatorOptions
    {
        public string JwtKey { get; set; }

        public long MinutesToExpire { get; set; }
    }
}