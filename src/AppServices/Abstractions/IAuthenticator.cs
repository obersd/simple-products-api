namespace AppServices.Abstractions
{
    using System;
    using System.Security.Claims;
    using Microsoft.IdentityModel.Tokens;

    /// <summary>
    /// Exposes functionalities for reading, transforming, encoding and decoding JWT tokens.
    /// </summary>
    public interface IAuthenticator
    {
        /// <summary>
        /// Encodes a JWT token. 
        /// </summary>
        string EncodeToken(Func<dynamic> serializationFunc);

        /// <summary>
        /// Creates a principal object.
        /// </summary>
        ClaimsPrincipal CreatePrincipal(SecurityToken securityToken);
    }
}