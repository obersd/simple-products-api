namespace AppServices.Abstractions
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Entities.DataModel;

    /// <summary>
    /// Product app service.
    /// </summary>
    public interface IProductAppService
    {
        /// <summary>
        /// Creates a new product.
        /// </summary>
        Task SaveAsync(Product product);

        /// <summary>
        /// Gets all products.
        /// </summary>
        Task<IList<Product>> GetAllAsync();
    }
}