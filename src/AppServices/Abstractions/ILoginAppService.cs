namespace AppServices.Abstractions
{
    using System.Threading.Tasks;
    using Entities.DataModel;

    /// <summary>
    /// Login application services.
    /// </summary>
    public interface ILoginAppService
    {
        /// <summary>
        /// Gets user by password and username.
        /// </summary>
        Task<User> GetUserAsync(string username, string password);
    }
}