﻿namespace AppServices
{
    using System;
    using System.Text;
    using System.Threading.Tasks;
    using Abstractions;
    using BCrypt.Net;
    using Entities.DataModel;
    using Microsoft.EntityFrameworkCore;
    using Repositories;
    using Repositories.Contexts;

    /// <inheritdoc />
    public class LoginAppService : ILoginAppService
    {
        private readonly ProductsContext ctx;

        public LoginAppService(ProductsContext ctx) => this.ctx = ctx;

        /// <inheritdoc />
        public async Task<User> GetUserAsync(string username, string password)
        {
            var user = await this.ctx.Users.Include(x => x.Userroles).ThenInclude(x => x.Role)
                .FirstOrDefaultAsync(x => x.Email == username);
            if (user == null)
            {
                return null;
            }

            if (!BCrypt.Verify(Encoding.UTF8.GetString(Convert.FromBase64String(password)), user.Pwd))
            {
                return null;
            }

            this.ctx.Entry(user).State = EntityState.Detached;
            user.Pwd = string.Empty;
            return user;
        }
    }
}