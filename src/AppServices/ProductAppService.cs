﻿namespace AppServices
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Abstractions;
    using Entities.DataModel;
    using Microsoft.EntityFrameworkCore;
    using Repositories;
    using Repositories.Contexts;

    /// <inheritdoc />
    public sealed class ProductAppService : IProductAppService
    {
        private readonly ProductsContext ctx;

        public ProductAppService(ProductsContext ctx) => this.ctx = ctx;

        public async Task SaveAsync(Product product)
        {
            await this.ctx.Products.AddAsync(product);
            await this.ctx.SaveChangesAsync();
        }

        public async Task<IList<Product>> GetAllAsync() => await this.ctx.Products.ToListAsync();
    }
}