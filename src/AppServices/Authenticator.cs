namespace AppServices
{
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Security.Claims;
    using System.Text;
    using Abstractions;
    using Entities.Security;
    using Microsoft.Extensions.Options;
    using Microsoft.IdentityModel.Tokens;

    /// <inheritdoc />
    public class Authenticator : IAuthenticator
    {
        public const string AuthenticationDefaultValue = "AlexVargas";
        private readonly JwtSecurityTokenHandler jwtSecurityTokenHandler;
        private readonly long minutesToExpiration;
        private readonly SecurityKey securityKey;

        public Authenticator(JwtSecurityTokenHandler jwtSecurityTokenHandler,
            IOptionsSnapshot<AuthenticatorOptions> options)
        {
            this.jwtSecurityTokenHandler = jwtSecurityTokenHandler;
            var opts = options.Get(nameof(Authenticator));
            this.minutesToExpiration = opts.MinutesToExpire;
            this.securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(opts.JwtKey));
        }

        /// <inheritdoc />
        public string EncodeToken(Func<dynamic> serializationFunc)
        {
            var payload = new JwtPayload
            {
                {"iat", DateTimeOffset.UtcNow.ToUnixTimeSeconds()},
                {"nbf", DateTimeOffset.UtcNow.ToUnixTimeSeconds()},
                {"exp", DateTimeOffset.UtcNow.AddMinutes(this.minutesToExpiration).ToUnixTimeSeconds()},
                {"sub", serializationFunc()},
                {"iss", "AlexVargas"},
            };

            return this.jwtSecurityTokenHandler.WriteToken(new JwtSecurityToken(
                new JwtHeader(new SigningCredentials(this.securityKey, SecurityAlgorithms.HmacSha256)), payload));
        }

        /// <inheritdoc />
        public ClaimsPrincipal CreatePrincipal(SecurityToken securityToken) =>
            new ClaimsPrincipal(this.CreateIdentity((JwtSecurityToken) securityToken));

        private ClaimsIdentity CreateIdentity(SecurityToken securityToken)
        {
            var jwtToken = (JwtSecurityToken) securityToken;
            var user = UserIdentity.CreateFromSubject(jwtToken.Subject);
            var identity = new ClaimsIdentity(AuthenticationDefaultValue, ClaimTypes.Name, ClaimTypes.Role)
            {
                BootstrapContext = new IdentityBootstrapContext(user)
            };

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString()),
                new Claim(ClaimTypes.Email, user.Email ?? string.Empty),
            };

            claims.AddRange(user.Authorities.Select(item => new Claim(ClaimTypes.Role, item.Abbreviation)));
            identity.AddClaims(claims);
            return identity;
        }
    }
}