namespace Entities.Security
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.Json;

    /// <summary>
    /// User identity. This object is used by the Universal Identity infrastructure. It allows to obtain information
    /// about the authenticated user by a request to the API. 
    /// </summary>
    public sealed class UserIdentity
    {
        public UserIdentity(long userId, string email, string token)
        {
            this.UserId = userId;
            this.Email = email;
            this.Authorities = new List<UserAuthority>();
            this.Token = token;
        }

        public long UserId { get; set; }

        public string Email { get; }

        public IList<UserAuthority> Authorities { get; }

        public string Token { get; }

        public void AddAuthority(UserAuthority userAuthority) => this.Authorities.Add(userAuthority);

        public dynamic Serialize() =>
            new
            {
                id = this.UserId,
                mail = this.Email,
                roles = this.Authorities.Select(x => new
                {
                    name = x.Name,
                    abbr = x.Abbreviation
                })
            };

        public static UserIdentity CreateFromSubject(string subject)
        {
            JsonElement userData = JsonSerializer.Deserialize<dynamic>(subject);
            var userIdentity = new UserIdentity(
                userData.TryGetProperty("id", out var userId) ? userId.GetInt64() : 0,
                userData.TryGetProperty("mail", out var email) ? email.GetString() : string.Empty,
                subject);

            if (!userData.TryGetProperty("roles", out var authorities))
            {
                return userIdentity;
            }

            var authoritiesArray = authorities.EnumerateArray();
            foreach (var item in authoritiesArray.Select(
                x => new UserAuthority(0,
                    x.TryGetProperty("name", out var abbr) ? abbr.GetString() : string.Empty,
                    x.TryGetProperty("abbr", out var name) ? name.GetString() : string.Empty)).ToList())
            {
                userIdentity.AddAuthority(item);
            }

            return userIdentity;
        }
    }
}