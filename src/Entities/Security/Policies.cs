namespace Entities.Security
{
    /// <summary>
    /// Contains policies, constraints and permissions required by authorization and CORS.
    /// </summary>
    public static class Policies
    {
        public const string CorsAllowOrigins = "AllowAllOrigins";
        public const string RolePolicyOnlyProd = "RequireRoleProduction";
        public const string RolePolicyAll = "RequireRoleAll";
    }
}