namespace Entities.Security
{
    /// <summary>
    /// The user's authority corresponds to a role defined in the business model.
    /// </summary>
    public sealed class UserAuthority
    {
        public UserAuthority(long authorityId, string name, string abbreviation)
        {
            this.AuthorityId = authorityId;
            this.Name = name;
            this.Abbreviation = abbreviation;
        }

        public long AuthorityId { get; set; }

        public string Name { get; set; }

        public string Abbreviation { get; set; }
    }
}