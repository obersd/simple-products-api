namespace Entities.Security
{
    /// <summary>
    /// Source object used to construct principal object.
    /// </summary>
    public class IdentityBootstrapContext
    {
        public IdentityBootstrapContext(UserIdentity userIdentity) => this.UserIdentity = userIdentity;

        public UserIdentity UserIdentity { get; }
    }
}