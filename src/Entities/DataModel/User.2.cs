namespace Entities.DataModel
{
    using System.Linq;
    using Security;

    public partial class User
    {
        public UserIdentity ToUserIdentity()
        {
            var user = new UserIdentity(this.UserId,  this.Email, string.Empty);
            foreach (var item in this.Userroles.Where(x => x.Role != null && x.RoleId > 0 && x.Role.RoleId > 0))
            {
                user.AddAuthority(new UserAuthority(item.RoleId, item.Role?.Name, item.Role?.Abbreviation));
            }

            return user;
        }
    }
}