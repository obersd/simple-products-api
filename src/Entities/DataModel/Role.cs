﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.DataModel
{
    public partial class Role
    {
        public Role()
        {
            Userroles = new HashSet<Userrole>();
        }

        public int RoleId { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }

        public virtual ICollection<Userrole> Userroles { get; set; }
    }
}
