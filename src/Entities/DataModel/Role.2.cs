namespace Entities.DataModel
{
    public partial class Role
    {
        public const string RoleProd = "PROD";
        public const string RoleRead = "READ";
    }
}