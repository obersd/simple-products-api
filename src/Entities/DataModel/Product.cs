﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.DataModel
{
    public partial class Product
    {
        public int ProductId { get; set; }
        public string Productkey { get; set; }
        public string Name { get; set; }
        public string Model { get; set; }
        public double Quantity { get; set; }
    }
}
