﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Entities.DataModel
{
    public partial class User
    {
        public User()
        {
            Userroles = new HashSet<Userrole>();
        }

        public int UserId { get; set; }
        public string Email { get; set; }
        public string Pwd { get; set; }

        public virtual ICollection<Userrole> Userroles { get; set; }
    }
}
