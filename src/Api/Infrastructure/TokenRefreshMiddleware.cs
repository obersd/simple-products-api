namespace Api.Infrastructure
{
    using System.Diagnostics.CodeAnalysis;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using AppServices.Abstractions;
    using Entities.Security;
    using Microsoft.AspNetCore.Http;

    /// <summary>
    /// This middleware extract access token and redecode a new access token with parametes updated.
    /// </summary>
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public sealed class TokenRefreshMiddleware
    {
        private const string Authorization = "Authorization";

        private readonly RequestDelegate next;

        public TokenRefreshMiddleware(RequestDelegate next) => this.next = next;

        public async Task Invoke(HttpContext context, IAuthenticator authenticator)
        {
            if (context.Request.Path.StartsWithSegments(("/api")))
            {
                if (context.Request.Headers.ContainsKey(Authorization) &&
                    (context.User?.Identity?.IsAuthenticated ?? false)
                    && context.User?.Identity is ClaimsIdentity
                    {
                        BootstrapContext: IdentityBootstrapContext bootstrapContext
                    })
                {
                    var newToken = authenticator.EncodeToken(() => bootstrapContext.UserIdentity.Serialize());
                    if (context.Response.Headers.ContainsKey(Authorization))
                    {
                        context.Response.Headers[Authorization] = newToken;
                    }
                    else
                    {
                        context.Response.Headers.Add(Authorization, newToken);
                    }
                }
            }

            await this.next(context);
        }
    }
}