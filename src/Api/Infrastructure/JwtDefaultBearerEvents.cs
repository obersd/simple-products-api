namespace Api.Infrastructure
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Threading;
    using System.Threading.Tasks;
    using AppServices.Abstractions;
    using Microsoft.AspNetCore.Authentication.JwtBearer;

    /// <summary>
    /// This class handles the process of extraction, validation and transformation JWT token into Principal and
    /// Identity objects.
    /// </summary>
    public class JwtDefaultBearerEvents : JwtBearerEvents
    {
        private const string Authorization = "Authorization";
        private const string FailureMessage = "Authentication token is required.";
        private readonly IAuthenticator authenticator;

        /// <inheritdoc />
        public JwtDefaultBearerEvents(IAuthenticator authenticator) => this.authenticator = authenticator;

        /// <inheritdoc />
        public override Task MessageReceived(MessageReceivedContext context)
        {
            if (!context.HttpContext.Request.Headers.ContainsKey(Authorization))
            {
                context.Fail(FailureMessage);
                return Task.CompletedTask;
            }

            var requestToken = context.HttpContext.Request.Headers[Authorization][0];
            var bearerPreffix = $"{JwtBearerDefaults.AuthenticationScheme} ";
            if (!requestToken.StartsWith(bearerPreffix, StringComparison.CurrentCultureIgnoreCase))
            {
                context.Fail(FailureMessage);
                return Task.CompletedTask;
            }

            context.Token = requestToken.Substring(bearerPreffix.Length);
            return Task.CompletedTask;
        }

        /// <inheritdoc />
        public override Task TokenValidated(TokenValidatedContext context)
        {
            var principal = this.authenticator.CreatePrincipal((JwtSecurityToken) context.SecurityToken);
            Thread.CurrentPrincipal = principal;
            context.HttpContext.User = principal;
            context.Principal = principal;
            context.Success();
            return Task.CompletedTask;
        }
    }
}