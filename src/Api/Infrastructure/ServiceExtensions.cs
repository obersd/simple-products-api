namespace Api.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Reflection;
    using System.Security.Claims;
    using System.Text;
    using A2net.Swagger.SwaggerService;
    using AppServices;
    using AppServices.Abstractions;
    using Entities.DataModel;
    using Entities.Security;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.IdentityModel.Tokens;
    using Model.MappingProfiles;
    using Repositories;
    using Repositories.Contexts;

    /// <summary>
    /// This extensions is used to perform additional configuration settings. 
    /// </summary>
    internal static class ServiceExtensions
    {
        private static void AddMainServices(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddAutoMapper(expression => { expression.AddProfile<MappingProfile>(); });
            services.AddScoped<JwtDefaultBearerEvents>();
            services.AddScoped<JwtSecurityTokenHandler>();
            services.AddScoped<ILoginAppService, LoginAppService>();
            services.AddScoped<IProductAppService, ProductAppService>();
            services.AddScoped<IAuthenticator, Authenticator>();
            services.Configure<AuthenticatorOptions>(nameof(Authenticator), configuration.GetSection("Secured"));
        }

        private static ClaimsIdentity PrimaryIdentitySelector(IEnumerable<ClaimsIdentity> identities)
        {
            if (identities == null)
            {
                return null;
            }

            var allIdentities = identities.ToList();
            var principalIdentity = allIdentities.FirstOrDefault(x => string.Equals(x.AuthenticationType,
                Authenticator.AuthenticationDefaultValue, StringComparison.OrdinalIgnoreCase));

            if (principalIdentity == null)
            {
                return allIdentities.Any() ? allIdentities.First() : null;
            }

            return principalIdentity;
        }

        private static void AddAuthenticationServices(this IServiceCollection services,
            IConfiguration configuration)
        {
            ClaimsPrincipal.PrimaryIdentitySelector = PrimaryIdentitySelector;
            services.AddAuthentication(
                options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultForbidScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultSignOutScheme = JwtBearerDefaults.AuthenticationScheme;
                }).AddJwtBearer(
                JwtBearerDefaults.AuthenticationScheme,
                options =>
                {
                    options.RequireHttpsMetadata = true;
                    options.SaveToken = true;
                    options.IncludeErrorDetails = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey =
                            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Secured:JwtKey"])),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero + TimeSpan.FromMilliseconds(1984),
                        RequireExpirationTime = true
                    };
                    options.EventsType = typeof(JwtDefaultBearerEvents);
                    options.Validate();
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(Policies.RolePolicyOnlyProd, policy => policy.RequireRole(Role.RoleProd));
                options.AddPolicy(Policies.RolePolicyAll, policy => policy.RequireRole(Role.RoleProd, Role.RoleRead));
            });
        }

        public static void AddApiServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ProductsContext>(
                builder =>
                {
                    builder.UseSqlServer(configuration.GetConnectionString("Db"));
#if DEBUG
                    builder.EnableSensitiveDataLogging();
#endif
                });

            services.AddMainServices(configuration);
            services.AddSwaggerService(new OpenApiOptions
            {
                Name = "v1",
                ApiDescription = "Documentation API",
                ApiTitle = "Products API",
                ApiVersion = "Alpha",
                ContactEmail = "avargass0200@egresado.ipn.mx",
                ContactName = "Hello!",
                ContactUrl = "https://lexvargas.com",
                XmlDocuumentationPaths = new[] {$"{Assembly.GetExecutingAssembly().GetName().Name}.xml"},
                OperationFilters = new List<KeyValuePair<object[], Type>>
                {
                    new KeyValuePair<object[], Type>(Array.Empty<object>(), typeof(ContentTypeFilter))
                }
            });
            services.AddAuthenticationServices(configuration);
        }
    }
}