namespace Api.Model.MappingProfiles
{
    using AutoMapper;
    using Entities.DataModel;
    using Requests;
    using Responses;

    /// <summary>
    /// Adds mapping functionality for DTOs and Data Model Entities.
    /// </summary>
    public sealed class MappingProfile : Profile
    {
        public MappingProfile()
        {
            this.CreateMap<AddProductDto, Product>()
                .ForMember(dest => dest.Model, opt => opt.MapFrom(src => src.Model))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Productkey, opt => opt.MapFrom(src => src.Productkey))
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Quantity))
                .ForAllOtherMembers(x => x.Ignore());

            this.CreateMap<Product, ProductDto>()
                .ForMember(dest => dest.Model, opt => opt.MapFrom(src => src.Model))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Productkey, opt => opt.MapFrom(src => src.Productkey))
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Quantity))
                .ForMember(dest => dest.ProductId, opt => opt.MapFrom(src => src.ProductId))
                .ForAllOtherMembers(x => x.Ignore());
        }
    }
}