namespace Api.Model.Requests
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// DTO for Add Product.
    /// </summary>
    public sealed class AddProductDto
    {
        /// <summary>
        /// Product key.
        /// </summary>
        [Required(ErrorMessage = "Campo requerido.")]
        [StringLength(16, MinimumLength = 1, ErrorMessage = "La longitud debe ser entre 1 y 16.")]
        public string Productkey { get; set; }

        /// <summary>
        /// Product name.
        /// </summary>
        [Required(ErrorMessage = "Campo requerido.")]
        [StringLength(40, MinimumLength = 1, ErrorMessage = "La longitud debe ser entre 1 y 40.")]
        public string Name { get; set; }

        /// <summary>
        /// Product model.
        /// </summary>
        [Required(ErrorMessage = "Campo requerido.")]
        [StringLength(64, MinimumLength = 1, ErrorMessage = "La longitud debe ser entre 1 y 64.")]
        public string Model { get; set; }

        /// <summary>
        /// Quantity.
        /// </summary>
        [Required(ErrorMessage = "Campo requerido.")]
        [Range(0.01, 999_999_999_999, ErrorMessage = "El valor de este campo debe ser entre 0.01 y 999,999,999,999.")]
        public double? Quantity { get; set; }
    }
}