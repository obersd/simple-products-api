namespace Api.Model.Requests
{
    using System.ComponentModel.DataAnnotations;
    using Annotations;

    /// <summary>
    /// DTO for user login. 
    /// </summary>
    public sealed class RawLoginDto
    {
        /// <summary>
        /// User email.
        /// </summary>
        [Email(ErrorMessage = "El formato de correo electrónico es inválido.")]
        [Required(ErrorMessage = "Campo requerido.")]
        [StringLength(100, MinimumLength = 8, ErrorMessage = "La longitud debe ser entre 8 y 16.")]
        public string Username { get; set; }

        /// <summary>
        /// Password in base64 format.
        /// </summary>
        [Required(ErrorMessage = "Campo requerido.")]
        [StringLength(32, MinimumLength = 3, ErrorMessage = "La longitud debe ser entre 3 y 32.")]
        public string Pwd { get; set; }
    }
}