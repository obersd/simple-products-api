namespace Api.Model.Responses
{
    /// <summary>
    /// Response when login is OK.
    /// </summary>
    public sealed class TokenDto
    {
        public TokenDto(string token)
        {
            this.Token = token;
        }

        /// <summary>
        /// JWT token.
        /// </summary>
        public string Token { get; }
    }
}