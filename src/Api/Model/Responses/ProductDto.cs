namespace Api.Model.Responses
{
    /// <summary>
    /// DTO for list of products. 
    /// </summary>
    public class ProductDto
    {
        /// <summary>
        /// Product Id.
        /// </summary>
        public int ProductId { get; set; }
        
        /// <summary>
        /// Product key
        /// </summary>
        public string Productkey { get; set; }
        
        /// <summary>
        /// Product name.
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Product model.
        /// </summary>
        public string Model { get; set; }
        
        /// <summary>
        /// Quantity.
        /// </summary>
        public double Quantity { get; set; }
    }
}