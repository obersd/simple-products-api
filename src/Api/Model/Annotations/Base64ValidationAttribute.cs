namespace Api.Model.Annotations
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Validates if input is in base64 format.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class Base64ValidationAttribute : ValidationAttribute
    {
        /// <inheritdoc />
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var valueAsString = value?.ToString() ?? string.Empty;
            try
            {
                var _ = Convert.FromBase64String(valueAsString);
            }
            catch 
            {
                return new ValidationResult(this.ErrorMessage, new[] {validationContext.MemberName});
            }

            return ValidationResult.Success;
        }
    }
}