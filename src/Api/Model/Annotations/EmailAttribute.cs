namespace Api.Model.Annotations
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using A2net.Base.Helpers;

    /// <summary>
    /// Validates if input is in email address format.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class EmailAttribute : ValidationAttribute
    {
        /// <inheritdoc />
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var stringValue = value?.ToString();
            return BasicValidator.Match(stringValue, BasicValidator.EmailPattern)
                ? ValidationResult.Success
                : new ValidationResult(this.ErrorMessage, new[] {validationContext.MemberName});
        }
    }
}