namespace Api.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using A2net.WebHelpers.Responses;
    using AppServices.Abstractions;
    using AutoMapper;
    using Entities.DataModel;
    using Entities.Security;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Cors;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Model.Requests;

    /// <summary>
    /// Manages products for this application.
    /// </summary>
    [ApiController]
    [Route("api/products")]
    [Produces("application/json")]
    [Consumes("application/json")]
    [EnableCors(Policies.CorsAllowOrigins)]
    public class ProductController : ControllerBase
    {
        private readonly IProductAppService productAppService;
        private readonly IMapper mapper;

        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="productAppService">product app service.</param>
        /// <param name="mapper">Mapper.</param>
        public ProductController(IProductAppService productAppService, IMapper mapper)
        {
            this.productAppService = productAppService;
            this.mapper = mapper;
        }

        /// <summary>
        /// Creates a new product.
        /// </summary>
        /// <param name="dto">Data.</param>
        /// <response code="200">Product created.</response>
        /// <response code="400">Verify validations</response>
        /// <response code="401">Invalid credentials.</response>
        /// /// <response code="403">Forbidden.</response>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        [Route("")]
        [Authorize(Policy = Policies.RolePolicyOnlyProd)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = null)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = null)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        public async Task<IActionResult> Add(AddProductDto dto)
        {
            await this.productAppService.SaveAsync(this.mapper.Map<Product>(dto));
            return this.Ok();
        }

        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <response code="200">User logged in.</response>
        /// <response code="400">Verify validations</response>
        /// <response code="401">Invalid credentials.</response>
        /// <response code="403">Forbidden.</response>
        /// <returns>ActionResult.</returns>
        [HttpGet]
        [Route("")]
        [Authorize(Policy = Policies.RolePolicyAll)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = null)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = null)]
        [ProducesResponseType(StatusCodes.Status403Forbidden, Type = null)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        public async Task<IActionResult> All() =>
            this.Ok(this.mapper.Map<IEnumerable<Product>>(await this.productAppService.GetAllAsync()));
    }
}