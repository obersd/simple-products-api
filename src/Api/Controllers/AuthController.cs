namespace Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using A2net.WebHelpers.Responses;
    using AppServices.Abstractions;
    using Entities.Security;
    using Microsoft.AspNetCore.Cors;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Model.Requests;
    using Model.Responses;

    /// <summary>
    /// Login and JWT token issuance.
    /// </summary>
    [ApiController]
    [Route("api/auth")]
    [Produces("application/json")]
    [Consumes("application/json")]
    [EnableCors(Policies.CorsAllowOrigins)]
    public class AuthController : ControllerBase
    {
        private readonly ILoginAppService loginAppService;
        private readonly IAuthenticator authenticator;

        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="loginAppService">The loging app. service.</param>
        /// <param name="authenticator">The authenticator.</param>
        public AuthController(ILoginAppService loginAppService, IAuthenticator authenticator)
        {
            // var pwd = BCrypt.HashPassword("Aa.12345");
            this.loginAppService = loginAppService;
            this.authenticator = authenticator;
        }

        /// <summary>
        /// Performs login into the application.
        /// </summary>
        /// <param name="dto">Data.</param>
        /// <response code="200">User logged in.</response>
        /// <response code="400">Verify validations</response>
        /// <response code="401">Invalid credentials.</response>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        [Route("login")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<TokenDto>))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = null)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        public async Task<IActionResult> Login(LoginDto dto)
        {
            var user = await this.loginAppService.GetUserAsync(dto.Username, dto.Pwd);
            if (user == null)
            {
                return this.Unauthorized(null);
            }

            return this.Ok(new TokenDto(this.authenticator.EncodeToken(() => user.ToUserIdentity().Serialize())));
        }

        /// <summary>
        /// Performs login into the application.
        /// </summary>
        /// <param name="dto">Data.</param>
        /// <response code="200">User logged in.</response>
        /// <response code="400">Verify validations</response>
        /// <response code="401">Invalid credentials.</response>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        [Route("rawlogin")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<TokenDto>))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = null)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        public async Task<IActionResult> RawLogin(RawLoginDto dto)
        {
            var user = await this.loginAppService.GetUserAsync(dto.Username,
                Convert.ToBase64String(Encoding.UTF8.GetBytes(dto.Pwd)));
            if (user == null)
            {
                return this.Unauthorized(null);
            }

            return this.Ok(new TokenDto(this.authenticator.EncodeToken(() => user.ToUserIdentity().Serialize())));
        }
    }
}