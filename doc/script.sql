create database products;
use products;

drop table if exists userroles;
drop table if exists [user];
drop table if exists role;
drop table if exists product;

-- Representa y almacena las propiedades mínimas de un usuario.
create table [user](
    userId              integer identity not null primary key,  -- Llave primaria.
    email               nvarchar(64) not null,                  -- Correo electŕonico.
    pwd                 nvarchar(512) not null,                 -- Password computado con un algoritmo Hash.
);

-- Representa y almacena las propiedades mínimas de un rol.
create table role(
    roleId          integer identity not null primary key,  -- Llave primaria.
    name            nvarchar(128) not null,                 -- Nombre del rol.
    abbreviation    nvarchar(16) not null                   -- Abreviatura.
);

-- Representa y almacena la relación entre un usuario y los roles asignados a él.
create table userroles(
    userId integer not null constraint userroles_userid_fk references [user](userId),   -- Llave foránea.
    roleId integer not null constraint userroles_roleid_fk references role(roleId),     -- LLave foránea.
    primary key (userId, roleId)                                                        -- Llave primaria.
);

-- Representa y almacena productos
create table product(
    productId       integer identity not null primary key,  -- Llave primaria.
    productkey      nvarchar(16) not null,                  -- Clave de producto.
    name            nvarchar(40) not null,                  -- Nombre de producto.
    model           nvarchar(64) not null,                  -- Modelo de producto.
    quantity        float not null                          -- Cantidad
);

insert into [user] (email, pwd) values ('alex@yopmail.com','$2b$10$PJ/vFPF3bsaSmqp7Jm4luOQ56ihMfi.2Vq4R8.P4mQjGFA.UO37Lq');
insert into [user] (email, pwd) values ('user@yopmail.com','$2b$10$PJ/vFPF3bsaSmqp7Jm4luOQ56ihMfi.2Vq4R8.P4mQjGFA.UO37Lq');
insert into role (name, abbreviation) values ('PRODUCCION','PROD');
insert into role (name, abbreviation) values ('LECTURA','READ');
insert into userroles (userId, roleId) values (1,2);
insert into userroles (userId, roleId) values (2,1);


select * from [user] u
inner join userroles u2 on u.userId = u2.userId
inner join role r2 on u2.roleId = r2.roleId

