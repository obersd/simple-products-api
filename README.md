# simple-products-api

This API shows how to implements a simple products api with jwt login very fast!

## Users

The system allows to login these users:

1. UID: alex@yopmail.com, ROLE: READ, PWD: Aa.12345
2. UID: user@yopmail.com, ROLE: PROD, PWD: Aa.12345


## Database

The database has been created with Docker:

LINUX:
```bash
sudo docker pull mcr.microsoft.com/mssql/server:2019-latest
sudo docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=R12345678.A" -p 1433:1433 --name sql -h sql -d mcr.microsoft.com/mssql/server:2019-latest
sudo docker start sql && docker ps -a
```

WINDOWS:
```bash
docker pull mcr.microsoft.com/mssql/server:2019-latest
docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=R12345678.A" -p 1433:1433 --name sql -h sql -d mcr.microsoft.com/mssql/server:2019-latest
docker start sql
docker ps -a
```
Then execute a sql script called: doc/script.sql

## EntityFramework

Create the DbContext with:

```bash
dotnet ef dbcontext scaffold "Data Source=localhost;Database=products;User Id=sa;Password=R12345678.A;" "Microsoft.EntityFrameworkCore.SqlServer" -f -c ProductsContext --context-dir Contexts -o ../Entities/DataModel -n Entities.DataModel --context-namespace Repositories.Contexts -v --no-build --no-onconfiguring

```
